import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { publicationResolver } from './publication.resolver';

describe('publicationResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => publicationResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
