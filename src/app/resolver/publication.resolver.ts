import { inject } from '@angular/core';
import { ResolveFn } from '@angular/router';
import { PublicationService } from '../services/publication.service';


export const publicationResolver: ResolveFn<any> = (route, state) => {
  var id = route.paramMap.get('id');
  console.log("eto baina " + id);
  var inj = inject(PublicationService);
  var tet = inj.getPublicationById(id);
  return tet;
};