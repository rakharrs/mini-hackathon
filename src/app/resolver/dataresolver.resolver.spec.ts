import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { dataresolverResolver } from './dataresolver.resolver';

describe('dataresolverResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => dataresolverResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
