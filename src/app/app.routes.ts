import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DetailPublicationComponent } from './components/detail-publication/detail-publication.component';
import { LoginComponent } from './login/login.component';
import { ModalFormComponent } from './modal-form/modal-form.component';

import { ListArtsComponent } from './components/list-arts/list-arts.component';
import { dataresolverResolver } from './resolver/dataresolver.resolver';
import { publicationResolver } from './resolver/publication.resolver';

export const routes: Routes = [
  { path : "home", component : HomeComponent, resolve : {myData : dataresolverResolver} },
  { path : "list", component : ListArtsComponent, resolve : {myData : dataresolverResolver} },
  { path : "detail/:id",component : DetailPublicationComponent, resolve: {myData: dataresolverResolver, data: publicationResolver}},
  { path : "detail", component : DetailPublicationComponent, resolve : {myData : dataresolverResolver}},
  { path : "logintest", component : LoginComponent},
  { path : "modal", component : ModalFormComponent},
  { path: '**', redirectTo: '/home'}
];
