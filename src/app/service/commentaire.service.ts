import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentaireService {

  private baseUrl = 'http://192.168.88.239:8080';

  constructor( private http: HttpClient ) { }

  addComment( comment : any ) : void{
    let url = `${this.baseUrl}/interact/comment`;
    console.log(comment);
    this.http.post<any>( url, comment, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json'
      })
    } ).subscribe();

  }

}
