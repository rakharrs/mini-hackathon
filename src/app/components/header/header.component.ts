import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { PublicationService } from '../../services/publication.service';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {

  searchText: string = '';

  publications$!: Observable<any[]>;

  constructor( private publicationService: PublicationService ){}

  searchPublications(){
    this.publications$ = this.publicationService.searchPublications( this.searchText );
  }

  openFile(){
    let d = document.getElementById('image');
    console.log(d);
    if( d != null ){
      console.log('hell');
      d.click();
    }
  }

  handleChange( event : any ){
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onloadend = () =>{
      const b64 = reader.result as string;
      this.publicationService.searchPublicationsByPic(b64);
    };

    if(file){
      reader.readAsDataURL(file);
    }

  }


}
