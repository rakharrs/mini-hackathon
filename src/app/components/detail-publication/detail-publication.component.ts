import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
// import { map } from 'jquery';
import { Observable, map } from 'rxjs';
import { AutoResizeTextareaDirective } from '../../auto-resize-textarea.directive';
import { CommentaireService } from '../../service/commentaire.service';
import { PublicationService } from '../../services/publication.service';
import { HeaderComponent } from '../header/header.component';
import { ListArtsComponent } from '../list-arts/list-arts.component';
declare var $: any;
@Component({
  selector: 'app-detail-publication',
  standalone: true,
  imports: [AutoResizeTextareaDirective, HeaderComponent,ListArtsComponent, CommonModule, FormsModule],
  templateUrl: './detail-publication.component.html',
  styleUrl: './detail-publication.component.scss'
})
export class DetailPublicationComponent implements OnInit{

  publication$!: Observable<any>;

  currentComment: string = '';

  constructor(private publicationService: PublicationService, private route: ActivatedRoute,
    private comService: CommentaireService
    ){}

  fetchPublication(  ){
    this.publication$ = this.route.data.pipe( map( data => {
      console.log( data['data'] );
     return data['data'];
    }) );
  }

  comment( publication: string ){
    console.log('hahahahaha');
    let profile = localStorage.getItem('profil');
    let comments = document.querySelector("#comment-area");
    if(this.currentComment !== ''){
      console.log('====> ' + this.currentComment);
      let json = {
        profil: profile,
        publication: publication,
        action: 8,
        commentaire: this.currentComment
      };

      this.comService.addComment(JSON.stringify(json));

    }
  }

  ngOnInit(): void {
    console.log("eto be");
    this.fetchPublication();
    console.log( this.publication$ );
    var proCata = $('.amado-pro-catagory');
    var singleProCata = ".single-products-catagory";

    if ($.fn.imagesLoaded) {
        proCata.imagesLoaded(function () {
            proCata.isotope({
                itemSelector: singleProCata,
                percentPosition: true,
                masonry: {
                    columnWidth: singleProCata
                }
            });
        });
    }
  }

  animate(){
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
  }
}
