import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Observable, map } from 'rxjs';
import { PublicationService } from '../../services/publication.service';
declare var $: any;

import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-list-arts',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './list-arts.component.html',
  styleUrl: './list-arts.component.scss'
})

export class ListArtsComponent implements OnInit{

  // publications: any[] = [];

  @Input() searchedPubs$! : Observable<any[]>;

  publications$! : Observable<any[]>

  constructor( private publicationService: PublicationService , private activatedRoute: ActivatedRoute){

  }


  initMansoryLayout(){
    console.log("hahahaha 2");
    var proCata = $('.amado-pro-catagory');
    var singleProCata = ".single-products-catagory";

    if ($.fn.imagesLoaded) {
      console.log("eto eto");
        proCata.imagesLoaded(function () {
            proCata.isotope({
                itemSelector: singleProCata,
                percentPosition: true,
                // layoutMode: 'masonry',
                masonry: {
                    columnWidth: singleProCata
                }
            });
        });
  }
  }

  ngOnInit(): void{
    this.publications$ = this.activatedRoute.data.pipe( map(data => {console.log("asd" + data['myData']); return data['myData']}));
  }


}
