import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  private baseUrl = 'http://192.168.88.239:8080';

  constructor( private http: HttpClient ) { }

  getPublications() : Observable<any[]>{
    return this.http.get<any[]>(`${this.baseUrl}/publication`);
  }

  getPublication( id: number ) : Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/publication/${id}`);
  }

  getPublicationById( id: string | null ) : Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/publication/${id}`);
  }

  searchPublications(keywords: string) : Observable<any> {
    let url = `${this.baseUrl}/search`;
    let opt = {
      headers: new HttpHeaders({
        'Content-Type' : 'text/plain'
      })
    };
    return this.http.post<any>( url, keywords, opt );

  }

  searchPublicationsByPic(keywords: string) : Observable<any> {
    let url = `${this.baseUrl}/search/photo`;
    let opt = {
      headers: new HttpHeaders({
        'Content-Type' : 'text/plain'
      })
    };
    return this.http.post<any>( url, keywords, opt );

  }

}
