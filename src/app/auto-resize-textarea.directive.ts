
import { Directive, ElementRef, HostListener } from '@angular/core';
@Directive({
  selector: '[appAutoResizeTextarea]',
  standalone: true
})

export class AutoResizeTextareaDirective {
  constructor(private el: ElementRef) {}

  @HostListener('input')
  onInput() {
     this.resize();
  }

  resize() {
     const textarea = this.el.nativeElement;
     textarea.style.height = 'auto'; // Reset the height
     textarea.style.height = textarea.scrollHeight + 'px'; // Set the height to scrollHeight
  }
 }
